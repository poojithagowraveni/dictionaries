# Create a dictionary
student = {
    "name": "John",
    "age": 20,
    "grade": "A"
}

# Access values by key
print(student["name"]) 

# Modify values
student["age"] = 21
print(student)  

# Add new key-value pairs
student["city"] = "New York"
print(student)  

# Remove a key-value pair
del student["grade"]
print(student)  

# Dictionary length
length = len(student)
print(length)  

# Check if a key exists
print("age" in student)  

# Get all keys or values
keys = student.keys()
values = student.values()
print(keys) 
print(values)